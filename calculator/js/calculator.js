//Variables storing the two numbers of the equation
var firstNumber = "";
var secondNumber = "";
//Variable that keeps track of the original number when equals is pressed multiple times in a row after successfully completing an equation
var firstEquals = "";
//Variable that stores the current operand
var oper;
//Variable that stores the previous button press (for executing appropriate logic)
var prevButton = "c";

// array to populate the inputs
var arrayOperators = ["+", "-", "/", "*", "+/-"]; 
var arrayOtherActions = [ ".", "c", "="];
            
//Function called when any button on the calculator is pushed
function buttonPush(button){

            var displayInput = document.getElementById("displayInput");
            var number = button.value;
            //Switch statement is passed the value of the button pressed and executes appropriate logic based on the button that was pressed
            switch(number)
            {
                case "c":
                    displayInput.value = "0";
                    firstNumber = "";
                    secondNumber = "";
                    oper = "";
                    prevButton = "";
                break;

                case "+":
                    operatorPress("+");
                break;

                case "-":
                    operatorPress("-");
                break;

                case "*":
                    operatorPress("*");
                break;

                case "/":
                    operatorPress("/");
                break;

                case "+/-":
                    if(displayInput.value != "+" && displayInput.value != "-" && displayInput.value != "*" && displayInput.value != "/")
                    {
                        if(displayInput.value.includes("-"))
                        {
                            displayInput.value = displayInput.value.replace("-" , "");
                        }
                        else{
                            displayInput.value = "-" + displayInput.value;
                        }
                        if(prevButton == "=")
                        {
                            oper ="";
                            firstNumber = displayInput.value;
                            secondNumber = "";
                        }
                    }
                break;

                case "=":
                    //Check to see if previous button was an operand
                    if(prevButton == "+" || prevButton == "-" || prevButton == "/" || prevButton == "*")
                    {
                        //if the second part of the equation has not been entered then do nothing
                        if(secondNumber == ""){
                            return;
                        }
                    }
                    //if previous button was "=" then use firstEquals and the new second number to perform equation
                    if(prevButton == "=")
                    {
                        secondNumber += displayInput.value;
                        switch(oper)
                        {
                            case "+":
                            displayInput.value = (parseFloat(firstEquals) + parseFloat(secondNumber));
                            break;
                            case "-":
                            displayInput.value = (parseFloat(secondNumber) - parseFloat(firstEquals));
                            break;
                            case "*":
                            displayInput.value = (parseFloat(firstEquals) * parseFloat(secondNumber));
                            break;
                            case "/":
                            displayInput.value = (parseFloat(secondNumber) / parseFloat(firstEquals));
                            break;
                            default:
                            break;
                        }
                        //the second half of the equation is now set to the first number and the second half is reset to blank 
                        //in preparation for the next entry
                        firstNumber = secondNumber;
                        secondNumber = "";
                    }
                    //conventional calculator use (first number entered, operand entered, second number entered, pressed equals)
                    else{
                        secondNumber += displayInput.value;
                        switch(oper)
                        {
                            case "+":
                            displayInput.value = (parseFloat(firstNumber) + parseFloat(secondNumber));
                            break;
                            case "-":
                            displayInput.value = (parseFloat(firstNumber) - parseFloat(secondNumber));
                            break;
                            case "*":
                            displayInput.value = (parseFloat(firstNumber) * parseFloat(secondNumber));
                            break;
                            case "/":
                            displayInput.value = (parseFloat(firstNumber) / parseFloat(secondNumber));
                            break;
                            default:
                            break;
                        }
                        //the second half of the equation is now set to the first number and the second half is reset to blank 
                        //in preparation for the next entry
                        firstEquals = secondNumber;
                        firstNumber = secondNumber;
                        secondNumber = "";
                    }
                    
                    
                break;
                //If the button pressed is a number or a decimal
                default:
                //check to see if previous button was a non number(except decimal)
                    if(prevButton == "+" || prevButton == "-" || prevButton == "/" || prevButton == "*" || prevButton == "=" || prevButton == "c" || prevButton == "+/-")
                    {
                        if(prevButton == "=")
                        {
                            oper = ""
                        }
                        displayInput.value = number;
                    }
                    else{
                        //make sure the current number displayed can only have one decimal point
                        if(number == "." && displayInput.value.includes("."))
                        {
                            break;
                        }
                        displayInput.value += number;
                    }
                break;
            }

            prevButton = number;

        }

function operatorPress(operator){
    //if the last button pressed was =
    if(prevButton == "=")
    {
        firstNumber = displayInput.value;
        oper = operator;
        displayInput.value = operator;
    }
    //change current operator if no new values have been added
    if(displayInput.value == "+" || displayInput.value == "-" || displayInput.value == "/" || displayInput.value == "*")
    {
        displayInput.value = operator;
        oper = operator;
    }
    //when you wish to enter a longer equation with more operands
    else if(firstNumber != "" && prevButton != operator)
    {
        secondNumber += displayInput.value;
        switch(oper)
        {
            case "+":
                displayInput.value = (parseFloat(firstNumber) + parseFloat(secondNumber));
            break;
            case "-":
                displayInput.value = (parseFloat(firstNumber) - parseFloat(secondNumber));
            break;
            case "*":
                displayInput.value = (parseFloat(firstNumber) * parseFloat(secondNumber));
            break;
            case "/":
                displayInput.value = (parseFloat(firstNumber) / parseFloat(secondNumber));
            break;
            default:
            break;
        }
        oper = operator;
        firstNumber = displayInput.value;
        secondNumber = "";
    }
    //if only a first number has been entered
    else
    {
        firstNumber = displayInput.value;
        oper = operator;
        displayInput.value = operator;
    }
}

/* it generates the input buttons*/
    function loadInputs(){
                
        /* keyboard */
        for(i=0; i<10; i++){
            document.getElementById('keyboard').innerHTML += '<input type="button" value="' + i + '" onclick="buttonPush(this)">';
        }

        /* operators */
        for(i=0; i<arrayOperators.length; i++){
            document.getElementById('operators').innerHTML += '<input type="button" value="' + arrayOperators[i] + '"onclick="buttonPush(this)">';
        }

        /* other actions (c, ., =) */
        for(i=0; i<arrayOtherActions.length; i++){
            document.getElementById('keyboard').innerHTML += '<input type="button" value="' + arrayOtherActions[i] + '" onclick="buttonPush(this)">';
        }
    }