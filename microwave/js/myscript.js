// status 0 is stopped
// status 1 is running
// status 2 is paused
var status = 0;

var minutes = 0; // added by whl
var seconds = 0; // added by whl
var minutesStr = "";
var secondsStr = "";
var display= "";
var timer = ""; // alpha characters - whl
var timeSet = "";
var counter = 0;


//Sets interval for timer
function startTimer(timer) {
  counter = timer;
//  var stopped = false;
//    timer = duration, minutes, seconds;
  timeSet = setInterval(function () {
    timeStringNum = parseInt(timer, 10);

    seconds = timeStringNum % 60;  // changed from 100 to 60
    minutes = (timeStringNum - seconds) / 60;  // changed from 100 to 60

//    if(seconds >= 60){
//      seconds = seconds % 60;
//      minutes = minutes + 1;
//    }
        
    minutesStr = minutes < 10 ? "0" + minutes : minutes;
    secondsStr = seconds < 10 ? "0" + seconds : seconds;

// added by whl to switch from 99 to 59 countdown
//    if(seconds % 100 == 99){
//      secondsStr = "59";
//      timer = minutesStr + secondsStr;
//    }

    //display.textContent = minutes + ":" + seconds;
    document.getElementById("txtNumber").value = minutesStr + ":" + secondsStr;

    if (--timer < 0) {
//      timer = stopped;
      document.getElementById("time").innerHTML = "DONE";
      document.getElementById("sound2").play();
      document.getElementById("sound3").pause() 
      document.getElementById("glass").style.backgroundColor="lightblue";
      document.getElementById("inner-wrap").className = " ";
      clearTimeout(timeSet);
      display.value = "";
      status = 0;
    }

    if(timer > 0) {
      document.getElementById("sound3").play()
      document.getElementById("glass").style.backgroundColor="#e0edba";     
      document.getElementById("time").innerHTML = "COOKING";
      document.getElementById("inner-wrap").className = " pizza";
    }
    //console.log(timer);
    console.log(counter--);
  }, 1000);
}

// Timer Start Function
function timeStart() {
  /*
  var time = document.getElementById("txtNumber").value;

  // checks to see if display is not empty
  if(document.getElementById("txtNumber").value != ""){
    startTimer(time);
    status = 1; // running
  }
  */
  switch(status){
    case "0":
//      var time = document.getElementById("txtNumber").value;
      var enteredtime = document.getElementById("txtNumber").value;
      var seconds = enteredtime % 100;
      var minutes = (enteredtime - seconds) / 100;
      var time = minutes*60 + seconds;
      // checks to see if display is not empty
      if(document.getElementById("txtNumber").value != ""){
        startTimer(time);
        status = 1; // running
      }
      console.log("stopped - status: "+status);
      break;
    case "1":
      console.log("running - status: "+status);
      break;
    case "2":
      //microwave is paused. need to restart timer from current time
        startTimer(counter);
        status = 1; // running
      console.log("paused - status: "+status);
      break;
    default:
      console.log("in default - status: "+status);
      break;
  }
}

// Timer Stop Function
function timeStop() {
//  var stopped = true;
  timer = document.getElementById("txtNumber");
//  timer = stopped;
  clearTimeout(timeSet);
  if(document.getElementById("time").innerHTML == "COOKING"){
    document.getElementById("time").innerHTML = "STOP";
    document.getElementById("inner-wrap").className = " ";
    status = 2;
  }
  else if(document.getElementById("time").innerHTML == "STOP"){
    display.value = "";
    status = 0;
  }  
}

// Displays number to textbox
function addNumber(buttonElement){
//  display.value+=document.getElementById(buttonElement.id).value;
//  document.getElementById("txtNumber").value=display.value;
  switch(status){
    case "0":
      display.value+=document.getElementById(buttonElement.id).value;
      document.getElementById("txtNumber").value=display.value;
      console.log("stopped - status: "+status);
      break;
    case "1":
      console.log("running - status: "+status);
      break;
    case "2":
      console.log("paused - status: "+status);
      break;
    default:
      console.log("in default - status: "+status);
      break;
  }         
}

function setup(){
  display = document.getElementById("txtNumber");
}